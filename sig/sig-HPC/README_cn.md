# HPC SIG

## 背景

openEuler HPC SIG的成员已在HPC气象、制造、分子动力学等领域Top应用进行深入优化，在性能、精度方面取得不错的优化效果。同时开展了HPC众智活动，许多高校师生参与到HPC应用向openEuler的迁移工作中。

## 目标

通过建立openEuler HPC SIG小组，汇聚HPC开发者、应用者。让Top应用优化快速应用到科研领域，吸引高校、政企共同参与HPC软件生态构建，吸收不同领域学者的思路、思想，把HPC SIG建设成HPC领域的根据地，共同繁荣HPC+openEuler生态。

## SIG 组职责

- 在 openEuler 社区中添加对 HPC 开源软件 的支持
- 负责 HPC 相关软件包的规划、迁移和调优，并输出相应的迁移调优文档
- 及时响应用户反馈，解决相关问题
- HPC SIG 组所有相关的文档、会议、邮件列表、IRC 的管理

### 交付物

- 源码、patch和文档

## 成员

### Maintainers 列表

- 王哲[@jerrywang125](https://gitee.com/jerrywang125), [32422240@qq.com](mailto:32422240@qq.com)

### committers 列表

- 何健聪[@hejiancong557](https://gitee.com/hejiancong557), [hejiancong1@huawei.com](mailto:hejiancong1@huawei.com)

- 覃璐瑶[@luyao201](https://gitee.com/luyao201), [tanluyao@huawei.com](mailto:tanluyao@huawei.com)

- 刘思圆[@liusiyuanOD](https://gitee.com/liusiyuanOD), [liusiyuan17@huawei.com](mailto:liusiyuan17@huawei.com)

- 汤国庆[@stephon-tone](https://gitee.com/stephon-tone), [tangguoqing8@huawei.com](mailto:tangguoqing8@huawei.com)

- 张嘉临[@zhangjialin9](https://gitee.com/zhangjialin9), [zhangjialin9@huawei.com](mailto:zhangjialin9@huawei.com)

- 杨德志[@deathy](https://gitee.com/deathy), [mapleyeh@qq.com](mailto:mapleyeh@qq.com)

- 王晓朋[@daniceexi](https://gitee.com/daniceexi), [wangxiaopeng99@huawei.com](mailto:wangxiaopeng99@huawei.com)

- 方春林[@iotwins](https://gitee.com/iotwins), [fangchunlin@huawei.com](mailto:fangchunlin@huawei.com)

## 联系方式

- [邮件列表](dev@openeuler.org)
